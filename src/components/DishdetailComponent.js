import React,{Component}from 'react';
//import { Media } from 'reactstrap';
import { Card, CardImg, CardText, CardBody,
    CardTitle, Breadcrumb, BreadcrumbItem,Button,Row,Col,Label,Modal,ModalBody,ModalHeader} from 'reactstrap';
import {Control,LocalForm,Errors} from 'react-redux-form'
import { Link } from 'react-router-dom';
import { Loading } from './LoadingComponent';
import { baseUrl } from '../shared/baseUrl';
import { FadeTransform, Fade, Stagger } from 'react-animation-components';
//import CommentForm from './CommentForm';



   function RenderDish({dish}) {

        if (dish!= null){
            return(
                <FadeTransform
                in
                transformProps={{
                    exitTransform: 'scale(0.5) translateY(-50%)'
                }}>
                    <Card>
                        <CardImg top src={baseUrl + dish.image} alt={dish.name} />
                        <CardBody>
                            <CardTitle>{dish.name}</CardTitle>
                            <CardText>{dish.description}</CardText>
                        </CardBody>
                    </Card>
                </FadeTransform>
            )
        }
        else{
            return(
               <div></div>
           )
        }
    }
   function RenderComments({comments,postComment, dishId}){
        if(comments==null){
            return(
                <div></div>
            )
        }
        const com=comments.map((comment)=>{
            return(
                <Fade in>
                    <li key={comment.id}> 
                        <p>{comment.comment}</p>
                        <p>-- {comment.author},{new Intl.DateTimeFormat('en-US',{year:'numeric',month:'long',day:'2-digit'}).format(new Date(comment.date))}</p>    
                    </li>
                </Fade>

                

 
            )
        })
            return(
                <div>
                    <h4>Comments</h4>
                    <ul className='list-unstyled'>
                        <Stagger in>
                            {com}
                        </Stagger>
                     </ul>
                    <CommentForm dishId={dishId} postComment={postComment} />    
                </div>
        )
    }
    const DishDetail=(props)=>{
       if (props.dish == null) {
        return (<div></div>)
        }
        else if (props.isLoading) {
            return(
                <div className="container">
                    <div className="row">            
                        <Loading />
                    </div>
                </div>
            );
        }
        else if (props.errMess) {
            return(
                <div className="container">
                    <div className="row">            
                        <h4>{this.props.errMess}</h4>
                    </div>
                </div>
            );
        }
        else
            return(
                <div className="container">
                    <div className="row">
                        <Breadcrumb>
                            <BreadcrumbItem><Link to='/home'>Home</Link></BreadcrumbItem>
                            <BreadcrumbItem><Link to='/menu'>Menu</Link></BreadcrumbItem>  
                            <BreadcrumbItem active>{props.dish.name}</BreadcrumbItem>
                        </Breadcrumb>
                        <div className="col-12">
                            <h3>{props.dish.name}</h3>
                            <hr />
                        </div>                
                    </div>             
                    <div className="row">
                        <div className="col-12 col-md-5 m-1">
                            <RenderDish dish={props.dish} />
                        </div>
                        <div className="col-12 col-md-5 m-1">
                            <RenderComments comments={props.comments} postComment={props.postComment} dishId={props.dish.id} />
                        </div>
                    </div>
                </div>

            );
    }

export default DishDetail;

const required=(val)=>val&&val.length;
const maxLength=(len)=>(val)=>!(val)||(val.length<=len);
const minLength=(len)=>(val)=>(val)&&(val.length>=len);

export class CommentForm extends Component{
    constructor(props){
        super(props);
        this.toggleModal=this.toggleModal.bind(this);
        this.state={
            isModalOpen:false
        }
    }
    handleSubmit(values) {
        this.toggleModal();
        this.props.postComment(this.props.dishId,values.rating,values.name,values.comment);
    }
    toggleModal(){
        this.setState({
            isModalOpen:!this.state.isModalOpen
        })
    }
    render(){
        return(
            <div>
                <Modal isOpen={this.state.isModalOpen} toggle={this.toggleModal}>
                    <ModalHeader toggle={this.toggleModal}>Submit Comment</ModalHeader>
                    <ModalBody>
                        <LocalForm onSubmit={(values)=>this.handleSubmit(values)}>
                            <div className="form-group">
                                <Label htmlFor="rating" >Rating</Label>
                                <Control.select model=".rating" id="rating" name="rating" className="form-control">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                </Control.select>
                            </div>
                            <div className="for-group">
                                <Label htmlFor="name">Your Name</Label>
                                <Control.text model=".name" id="name" name="name" className="form-control" placeholder="Your Name" validators={{minLength:minLength(3),maxLength:maxLength(15)}}/><Errors className="text-danger" model=".name" show="touched" messages={{minLength: 'Must be greater than 2 characters' ,maxLength: 'Must be 15 characters or less'}}/>
                            </div>
                            <div className="form-group">
                                <Label htmlFor="comment">Comment</Label>
                                <Control.textarea model=".comment" id="comment" name="comment" className="form-control" rows="6"/>
                            </div>
                            <div className="form-group">
                                <Button type="submit" color="primary">
                                    Submit
                                </Button>
                            </div>
                        </LocalForm>
                    </ModalBody>
                </Modal>
                <Button outline onClick={this.toggleModal}><span className="fa fa-pencil fa-lg"></span>Submit Comment</Button>
            </div>
        )
    }
}
